<?php
/**
 * Created by PhpStorm.
 * User: jefinghelbrecht
 * Date: 08/02/2019
 * Time: 19:02
 */

namespace ModernWays\Vos\Model;
class User implements \ModernWays\Identity\IUser
{
    private $name;
    private $salt;
    private $hashedPassword;
    protected $personList;
    private $personId;
    protected $roleList;
    private $roleId;
    private $id;
    protected $isAuthenticated;

    protected $isDV;
    protected $isGuest;
    protected $isLDO;

    /**
     * @return mixed
     */
    public function getIsDV()
    {
        return $this->isDV;
    }

    /**
     * @param mixed $isDV
     */
    public function setIsDV($value)
    {
        $this->isDV = $value;
    }

    /**
     * @return mixed
     */
    public function getIsGuest()
    {
        return $this->isGuest;
    }

    /**
     * @param mixed $isGuest
     */
    public function setIsGuest($value)
    {
        $this->isCoach = $value;
    }

    /**
     * @return mixed
     */
    public function getIsLDO()
    {
        return $this->isLDO;
    }

    /**
     * @param mixed $isDV
     */
    public function setIsLDO($value)
    {
        $this->isLDO = $value;
    }

    public function __construct($isAuthenticated = false)
    {
        $this->isAuthenticated = $isAuthenticated;
    }

    /**
     * @return mixed
     */
    public function getIsAuthenticated()
    {
        return $this->isAuthenticated;
    }

    /**
     * @param mixed $isAuthenticated
     */
    public function setIsAuthenticated($isAuthenticated)
    {
        $this->isAuthenticated = $isAuthenticated;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getSalt()
    {
        return $this->salt;
    }

    public function getHashedPassword()
    {
        return $this->hashedPassword;
    }

    public function getPersonList()
    {
        return $this->personList;
    }

    public function getPersonId()
    {
        return $this->personId;
    }

    public function getRoleList()
    {
        return $this->roleList;
    }

    public function getRoleId()
    {
        return $this->roleId;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setName($value)
    {
        $this->name = $value;
    }

    public function setSalt($value)
    {
        $this->salt = $value;
    }

    public function setHashedPassword($value)
    {
        $this->hashedPassword = $value;
    }

    public function setPersonId($value)
    {
        $this->personId = $value;
    }

    public function setPersonList($value)
    {
        $this->personList = $value;
    }

    public function setRoleId($value)
    {
        $this->roleId = $value;
    }

    public function setRoleList($value)
    {
        $this->roleList = $value;
    }

    public function setId($value)
    {
        $this->id = $value;
    }

    public function set($name, $salt, $hashedPassword, $personId, $roleId, $id)
    {
        $this->setName($name);
        $this->setSalt($salt);
        $this->setHashedPassword($hashedPassword);
        $this->setId($id);
    }
}
