<?php
/**
 * Created by PhpStorm.
 * User: jefinghelbrecht
 * Date: 09/02/2019
 * Time: 10:33
 */

include ('../../../../autoload.php');

\ModernWays\Dal::$configLocation = __DIR__ . '/../config.ini';

try {
    $connection = \ModernWays\Dal::connect('global');
    $sql = file_get_contents("vos_ddl.sql");
    $connection->exec($sql);

    echo "Database and tables User and Role created successfully.";
} catch(\PDOException $error) {
    echo $sql . "<br>" . $error->getMessage();
}