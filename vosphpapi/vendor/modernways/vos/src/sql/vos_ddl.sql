--
SET GLOBAL TRANSACTION ISOLATION LEVEL SERIALIZABLE;
-- mode changes syntax and behavior to conform more closely to standard SQL.
-- It is one of the special combination modes listed at the end of this section.
SET GLOBAL sql_mode = 'ANSI';
-- If database does not exist, create the database
CREATE DATABASE IF NOT EXISTS Vos;
USE `Vos`;
-- With the MySQL FOREIGN_KEY_CHECKS variable,
-- you don't have to worry about the order of your
-- DROP and CREATE TABLE statements at all, and you can
-- write them in any order you like, even the exact opposite.
SET FOREIGN_KEY_CHECKS = 0;

-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql: CREATE TABLE Role
-- Created on Sunday 27th of January 2019 03:29:21 PM
--
DROP TABLE IF EXISTS `Role`;
CREATE TABLE `Role` (
`Name` NVARCHAR (50) NOT NULL,
`Id` INT NOT NULL AUTO_INCREMENT,
CONSTRAINT PRIMARY KEY(Id),
CONSTRAINT uc_Role_Name UNIQUE (Name));

-- modernways.be
-- created by an orm apart
-- Entreprise de modes et de manières modernes
-- MySql: CREATE TABLE User
-- Created on Sunday 27th of January 2019 03:29:21 PM
--
DROP TABLE IF EXISTS `User`;
CREATE TABLE `User` (
`Name` NVARCHAR (50) NOT NULL,
`Salt` NVARCHAR (255) NULL,
`HashedPassword` NVARCHAR (255) NULL,
`RoleId` INT NULL,
`Id` INT NOT NULL AUTO_INCREMENT,
CONSTRAINT PRIMARY KEY(Id),
CONSTRAINT uc_User_Name UNIQUE (Name),
CONSTRAINT fk_UserRoleId FOREIGN KEY (`RoleId`) REFERENCES `Role` (`Id`));

-- With the MySQL FOREIGN_KEY_CHECKS variable,
-- you don't have to worry about the order of your
-- DROP and CREATE TABLE statements at all, and you can
-- write them in any order you like, even the exact opposite.
SET FOREIGN_KEY_CHECKS = 1;
