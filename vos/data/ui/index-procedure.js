import React from "react";
import { goColor } from '../../components/helpers/home-style';
export const homeIndexProcedureData = [
  {
    iconName: "icon-fire",
    title: "Brand",
    backgroundColor: goColor.blue,
    color: "white",
    navigateTo: "FireIndex",
    code: "FI"
  },
  {
    iconName: "icon-accident-staircase",
    title: "Ongeval",
    backgroundColor: goColor.orange,
    color: "white",
    navigateTo: "AccidentIndex",
    code: "AI"
  },
  {
    iconName: "icon-poisonous-gas",
    title: "Gaslek",
    backgroundColor: goColor.green,
    color: "white",
    navigateTo: "GasLeakIndex",
    code: "GL"
  },
  {
    iconName: "icon-battle",
    title: "AMOK en blind geweld",
    backgroundColor: goColor.darkgray,
    color: "white",
    navigateTo: "ViewProcedure",
    code: "AMOK"
  },
  {
    iconName: "icon-terrorism",
    title: "Terreur dreiging",
    backgroundColor: goColor.blue,
    color: "white",
    navigateTo: "TerrorIndex",
    code: "IT"
  },
  {
    iconName: "icon-mental-health",
    title: "Psycho-sociaal risico",
    backgroundColor: goColor.orange,
    color: "white",
    navigateTo: "PsychoSocialRiskIndex",
    code: "IP"
  },
  {
    iconName: "icon-radicalisation",
    title: "Radicalisering",
    backgroundColor: goColor.green,
    color: "white",
    navigateTo: "IndexFire",
    code: "IR"
  }
];

export const fireIndexProcedureData = [
  {
    iconName: "icon-alarm-button",
    title: "Brandmelding",
    backgroundColor: goColor.blue,
    color: "white",
    navigateTo: "ViewProcedure",
    code: "BM"
  },
  {
    iconName: "icon-evacuation-dark",
    title: "Evacuatie brand",
    backgroundColor: goColor.orange,
    color: "white",
    navigateTo: "ViewProcedure",
    code: "BREV"
  }
];

export const gasLeakIndexProcedureData = [
  {
    iconName: "icon-alarm-button",
    title: "Melding gaslek",
    backgroundColor: goColor.blue,
    color: "white",
    navigateTo: "ViewProcedure",
    code: "GLM"
  },
  {
    iconName: "icon-evacuation-dark",
    title: "Evacuatie gaslek",
    backgroundColor: goColor.orange,
    color: "white",
    navigateTo: "ViewProcedure",
    code: "BREV"
  }
];

export const terrorIndexProcedureData = [
  {
    iconName: "icon-bomb",
    title: "Bomalarm",
    backgroundColor: goColor.blue,
    color: "white",
    navigateTo: "BombAlarmIndex",
    code: "BA"
  },
  {
    iconName: "icon-suspicious-object",
    title: "Verdacht voorwerp",
    backgroundColor: goColor.orange,
    color: "white",
    navigateTo: "SuspiciousObjectIndex",
    code: "ISO"
  },
  {
    iconName: "icon-terrorist1",
    title: "Terroristische aanslag",
    backgroundColor: goColor.green,
    color: "white",
    navigateTo: "ViewProcedure",
    code: "TA"
  }
];

export const bombAlarmIndexProcedureData = [
  {
    iconName: "icon-alarm-button",
    title: "Melding bomalarm",
    backgroundColor: goColor.blue,
    color: "white",
    navigateTo: "ViewProcedure",
    code: "VVA"
  },
  {
    iconName: "icon-evacuation-dark",
    title: "Evacuatie bomalarm",
    backgroundColor: goColor.orange,
    color: "white",
    navigateTo: "ViewProcedure",
    code: "BAE"
  }
];

export const accidentIndexProcedureData = [
  {
    iconName: "icon-schoolbus",
    title: "Van en naar school/reis",
    backgroundColor: goColor.blue,
    color: "white",
    navigateTo: "ViewProcedure",
    code: "VENS"
  },
  {
    iconName: "icon-accident",
    title: "Ernstig ongeval",
    backgroundColor: goColor.orange,
    color: "white",
    navigateTo: "ViewProcedure",
    code: "EO"
  }
];

export const suspiciousObjectIndexProcedureData = [
  {
    iconName: "icon-suspicious-object-found",
    title: "ontdekken",
    backgroundColor: goColor.blue,
    color: "white",
    navigateTo: "ViewProcedure",
    code: "VVO"
  },
  {
    iconName: "icon-message-in",
    title: "ontvangen",
    backgroundColor: goColor.orange,
    color: "white",
    navigateTo: "ViewProcedure",
    code: "VPO"
  },
  {
    iconName: "icon-suspicious-object",
    title: "vaststellen?",
    backgroundColor: goColor.green,
    color: "white",
    navigateTo: "ViewProcedure",
    code: "VVW"
  },
  {
    iconName: "icon-alarm-button",
    title: "Alarm geven",
    backgroundColor: goColor.darkgray,
    color: "white",
    navigateTo: "ViewProcedure",
    code: "VVA"
  },
  {
    iconName: "icon-evacuation-dark",
    title: "Evacuatie",
    backgroundColor: goColor.blue,
    color: "white",
    navigateTo: "ViewProcedure",
    code: "BAE"
  }
];

export const psychoSocialRiskIndexProcedureData = [
  {
    iconName: "icon-sexual-assault",
    title: "Ongewenst gedrag",
    backgroundColor: goColor.blue,
    color: "white",
    navigateTo: "ViewProcedure",
    code: "PE"
  },
  {
    iconName: "icon-teasing-boy",
    title: "Pesten",
    backgroundColor: goColor.orange,
    color: "white",
    navigateTo: "ViewProcedure",
    code: "PE"
  },
  {
    iconName: "icon-burn-out",
    title: "Burn-out",
    backgroundColor: goColor.green,
    color: "white",
    navigateTo: "ViewProcedure",
    code: "PE"
  },
  {
    iconName: "icon-shock",
    title: "Schokkende gebeurtenis",
    backgroundColor: goColor.darkgray,
    color: "white",
    navigateTo: "ViewProcedure",
    code: "SG"
  }
];