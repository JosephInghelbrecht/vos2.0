/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import {AppRegistry} from 'react-native';
import app from './components/vos';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => app);
