import React, { Component } from "react";
import {
  tileStyle,
  scaledSize,
  homeStyle,
  iconTile,
  buttonStyle
} from "./helpers/home-style";
import { Footer, getDistanceFromLatLonInKm } from "./helpers/basic-elements";
import { Text, View, ScrollView, TouchableOpacity, Alert } from "react-native";
import IconFont from "./helpers/icon-font";
import identityData from "../data/identity.json";
import positionData from "../data/position.json";
import organisationList from "../data/organisationList";
import organisationData from "../data/organisation";
import AsyncStorage from '@react-native-community/async-storage';

class IndexProcedure extends Component {
  constructor(props) {
    super(props);

    this.state = {
      identityData: identityData,
      organisationData: organisationData
    };
  }

  componentDidMount = () => {
    this.getIdentityData();
    this.getGeolocation();
  }

  getIdentityData = async () => {
    try {
      const value = await AsyncStorage.getItem('@identityData')
      if (value !== null) {
        this.setState({ identityData: JSON.parse(value) });
      }
    } catch (e) {
      // error reading value
    }
  }

  setIdentityData = (value) => {
    AsyncStorage.setItem('@identityData', JSON.stringify(value));
    this.setState({ identityData: value });
  }

  getGeolocation() {
     navigator.geolocation.getCurrentPosition(
      (position) => {
        this.setMyOrganisation(position.coords.latitude,
          position.coords.longitude);
      },
      (error) => {
        this.setMyOrganisation(positionData.latitude,
          positionData.longitude);
      },
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
    );
  }

  logOut() {
    this.setIdentityData(identityData);
  }

  logButton() {
    if (this.state.identityData.loggedIn) {
      return (
        <TouchableOpacity style={buttonStyle.container}
          onPress={() => this.logOut()}>
          <Text style={buttonStyle.text}>Afmelden</Text>
        </TouchableOpacity>
      )
    } else {
      return (
        <TouchableOpacity style={buttonStyle.container}
          onPress={() => this.props.navigation.navigate("ViewProcedure", {
            code: 'GDPR',
            action: this.setIdentityData
          })}>
          <Text style={buttonStyle.text}>Aanmelden</Text>
        </TouchableOpacity>
      )
    }
  }

  /**
   * De dichtsbijzijnde organisatie ophalen.
   * https://stackoverflow.com/questions/21279559/geolocation-closest-locationlat-long-from-my-position
   */
  setMyOrganisation(latitude, longitude) {
    organisationList.forEach(function (item) {
      item.distanceFromMyLocation = getDistanceFromLatLonInKm(
        latitude, longitude,
        item.latitude, item.longitude);
    });
    organisationList.sort(function (a, b) {
      return a.distanceFromMyLocation - b.distanceFromMyLocation;
    });
    const myOrganisation = organisationList[0];
    this.setState({ organisationData: myOrganisation });
  }

  render() {
    /* hoe je state doorgeeft: 
       https://stackoverflow.com/questions/38689648/react-how-to-pass-state-to-another-component */
    /* 2. Get the param, provide a fallback value if not available */
    const { navigation } = this.props;
    const index = navigation.getParam("index", "NO-INDEX");
   
    const title = navigation.getParam("title", "VOS")
    return (
      <View style={homeStyle.container}>
        <View>
          <Text style={homeStyle.welcome}>{title}</Text>
          <Text style={homeStyle.xtext}>{this.state.identityData.userName}</Text>
          <Text style={homeStyle.text}>{this.state.identityData.function}</Text>
          <Text style={homeStyle.text}>{this.state.organisationData.name}</Text>
        {this.logButton()}
        </View>
        <ScrollView style={{ flex: 0.8 }}>
          {index.map((procedure, index) => {
            let counter = index;
            counter++;
            return(
            <TouchableOpacity
              // moet een unique key attribuut hebben in iteratie
              key={counter.toString()}
              onPress={() =>
                this.props.navigation.navigate(procedure.navigateTo, {
                  code: procedure.code,
                  title: procedure.title,
                  identityData: this.state.identityData,
                  organisationData: this.state.organisationData
                })
              }
            >
              <View style={tileStyle(procedure.backgroundColor)}>
                <IconFont
                  iconName={procedure.iconName}
                  fontSize={iconTile.fontSize}
                  color={iconTile.color}
                  backgroundColor={procedure.backgroundColor}
                />
                <Text style={homeStyle.tileTitle}>{procedure.title}</Text>
              </View>
            </TouchableOpacity>
          )})}
          <Footer />
        </ScrollView>
      </View >
    );
  }
}

export default IndexProcedure;
