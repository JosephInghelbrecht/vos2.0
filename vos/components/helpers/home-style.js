import React from 'react';
import { Dimensions, StyleSheet } from 'react-native';

export const goColor = {
    magenta: '#C60450',
    darkgray: '#737373',
    lightgray: '#C8C8C8',
    blue: '#00A4C8',
    green: '#86A92F',
    orange: '#dd882e'
}

export const { width, height } = Dimensions.get('window');

// Use iPhone6 as base size which is 375 x 667
const baseWidth = 375;
const baseHeight = 667;

const scaleWidth = width / baseWidth;
const scaleHeight = height / baseHeight;
const scale = Math.min(scaleWidth, scaleHeight);

export const scaledSize =
    (size) => Math.ceil((size * scale));

export const hexToAscii =
    (hexx) => {
        const hex = hexx.toString(); //force conversion
        let str = '';
        for (let i = 0; (i < hex.length && hex.substr(i, 2) !== '00'); i += 2)
            str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
        return str;
    }

export const iconTile = {
    fontSize: scaledSize(80),
    color: 'white'
};

export const tileStyle = (backgroundColor) => {
    return {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: backgroundColor,
        marginBottom: scaledSize(10),
        padding: scaledSize(6),
        borderRadius: scaledSize(5)
    }
};

export const homeStyle = StyleSheet.create({
    container: {
        backgroundColor: '#F5FCFF',
        marginLeft: scaledSize(10),
        marginRight: scaledSize(10),
        flexDirection: 'column',
        flex: 1
    },
    tile: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'red',
        marginTop: scaledSize(10),
        padding: 6,
        borderRadius: scaledSize(5)
    },
    tileTitle: {
        fontSize: scaledSize(26),
        color: 'white',
        fontWeight: "700"
    },
    welcome: {
        fontSize: scaledSize(26),
        textAlign: 'center',
        color: goColor.magenta
    },
    textInput: {
        height: scaledSize(40),
        backgroundColor: 'rgba(225,225,225,0.2)',
        marginBottom: scaledSize(10),
        padding: scaledSize(10),
        color: '#000'
    },
    labelText: {
        color: '#0d8898',
        fontSize: 20
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    text: {
        color: goColor.darkgray,
        textAlign: 'center',
        fontSize: scaledSize(14),
        marginBottom: scaledSize(7)
    },
    xtext: {
        color: goColor.darkgray,
        textAlign: 'center',
        fontSize: scaledSize(16),
        marginBottom: scaledSize(8)
    }
});

export const buttonStyle = StyleSheet.create({
    container: {
        backgroundColor: goColor.magenta,
        padding: scaledSize(7),
        borderRadius: scaledSize(10),
        marginTop: scaledSize(1),
        marginBottom: scaledSize(10),
        marginLeft: scaledSize(60),
        marginRight: scaledSize(60)
    },
    text: {
        color: '#fff',
        textAlign: 'center',
        fontWeight: '700',
        fontSize: scaledSize(20)
    }
});


