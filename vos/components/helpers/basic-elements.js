import React from "react";
import { Text, View } from "react-native";
import { homeStyle } from "./home-style";

export const Label = props => {
  return <Text style={homeStyle.labelText}>{props.title}</Text>;
};

export const HeaderOld = props => {
  return (
    <View>
        <Text style={homeStyle.welcome}>{props.title}</Text>
    </View>
  );
};

export const Footer = () => {
    return (
      <View>
        <Text style={homeStyle.text}> {"\u00A9"} Scholengroep Antigon</Text>
        <Text style={homeStyle.text}> realisatie ModernWays.be</Text>
      </View>
    );
  };

  export const getDistanceFromLatLonInKm = (lat1, lon1, lat2, lon2) => {
    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(lat2-lat1);  // deg2rad below
    var dLon = deg2rad(lon2-lon1);
    var a =
        Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
        Math.sin(dLon/2) * Math.sin(dLon/2)
    ;
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    var d = R * c; // Distance in km
    // alert('lat1' + lat1 + ' lat2: ' + lat2 + ' d: '+  d);
    return d;
}

function deg2rad(deg) {
    return deg * (Math.PI/180)
}

export const phoneCall = function(number) {
  
};
