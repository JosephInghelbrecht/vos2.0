import React, {Component} from 'react';
import {StyleSheet, Text} from 'react-native';
import {scaledSize, hexToAscii} from './home-style/';

const styles = StyleSheet.create({
    small: {
        fontFamily: 'tower-pattern',
        fontSize: scaledSize(50)
    },
    big: {
        fontFamily: 'tower-pattern',
        fontSize: scaledSize(100)
    }
});

const icons = [
    {name: 'icon-paperplane', content: '21'},
    {name: 'icon-send', content: '22'},
    {name: 'icon-ruler', content: '23'},
    {name: 'icon-enter', content: '24'},
    {name: 'icon-exit', content: '25'},
    {name: 'icon-pencil', content: '26'},
    {name: 'icon-image', content: '27'},
    {name: 'icon-images', content: '28'},
    {name: 'icon-book', content: '29'},
    {name: 'icon-books', content: '2a'},
    {name: 'icon-file', content: '2b'},
    {name: 'icon-copy', content: '2c'},
    {name: 'icon-stack', content: '2d'},
    {name: 'icon-folder', content: '2e'},
    {name: 'icon-folder-open', content: '2f'},
    {name: 'icon-notebook', content: '30'},
    {name: 'icon-disk', content: '31'},
    {name: 'icon-user', content: '32'},
    {name: 'icon-search', content: '33'},
    {name: 'icon-key', content: '34'},
    {name: 'icon-minus', content: '35'},
    {name: 'icon-plus', content: '36'},
    {name: 'icon-arrow-up', content: '37'},
    {name: 'icon-arrow-right', content: '38'},
    {name: 'icon-arrow-down', content: '39'},
    {name: 'icon-arrow-left', content: '3a'},
    {name: 'icon-bold', content: '3b'},
    {name: 'icon-underline', content: '3c'},
    {name: 'icon-italic', content: '3d'},
    {name: 'icon-paragraph-left', content: '3e'},
    {name: 'icon-paragraph-center', content: '3f'},
    {name: 'icon-paragraph-right', content: '40'},
    {name: 'icon-list', content: '41'},
    {name: 'icon-numbered-list', content: '42'},
    {name: 'icon-menu', content: '43'},
    {name: 'icon-menu2', content: '44'},
    {name: 'icon-print', content: '45'},
    {name: 'icon-remove', content: '46'},
    {name: 'icon-link', content: '47'},
    {name: 'icon-scissors', content: '48'},
    {name: 'icon-close', content: '49'},
    {name: 'icon-pencil2', content: '4a'},
    {name: 'icon-undo', content: '4b'},
    {name: 'icon-redo', content: '4c'},
    {name: 'icon-file-xml', content: '4d'},
    {name: 'icon-file-css', content: '4e'},
    {name: 'icon-html5', content: '4f'},
    {name: 'icon-css3', content: '50'},
    {name: 'icon-mail', content: '51'},
    {name: 'icon-indent-increase', content: '52'},
    {name: 'icon-indent-decrease', content: '53'},
    {name: 'icon-table', content: '54'},
    {name: 'icon-paste', content: '55'},
    {name: 'icon-check-ok', content: '56'},
    {name: 'icon-opt', content: '57'},
    {name: 'icon-users', content: '58'},
    {name: 'icon-compass', content: '59'},
    {name: 'icon-wrench', content: '5a'},
    {name: 'icon-cogs', content: '5b'},
    {name: 'icon-truck', content: '5c'},
    {name: 'icon-shock', content: '5d'},
    {name: 'icon-fire', content: '5e'},
    {name: 'icon-gas_mask', content: '5f'},
    {name: 'icon-accident-staircase', content: '60'},
    {name: 'icon-battle', content: '61'},
    {name: 'icon-fire-exit', content: '62'},
    {name: 'icon-mental-health', content: '63'},
    {name: 'icon-psycho', content: '64'},
    {name: 'icon-teasing-boy', content: '65'},
    {name: 'icon-terrorism', content: '66'},
    {name: 'icon-upload', content: '67'},
    {name: 'icon-poisonous-gas', content: '68'},
    {name: 'icon-radicalisation', content: '69'},
    {name: 'icon-evacuation-dark', content: '70'},
    {name: 'icon-phone', content: '71'},
    {name: 'icon-terrorist1', content: '72'},
    {name: 'icon-bomb', content: '73'},
    {name: 'icon-suspicious-object', content: '74'},
    {name: 'icon-schoolbus', content: '75'},
    {name: 'icon-accident', content: '76'},
    {name: 'icon-helmet-gear', content: '77'},
    {name: 'icon-extra-muros', content: '78'}, 
    {name: 'icon-burn-out', content: '79'},
    {name: 'icon-sexual-assault', content: '7a'},
    {name: 'icon-sms', content: '7b'},
    {name: 'icon-alarm-button', content: '7c'},
    {name: 'icon-alarm', content: '7d'},
    {name: 'icon-fire-call', content: '7e'},
    {name: 'icon-suspicious-object-found', content: '29'},
    {name: 'icon-suspicious-mail', content: '28'},
    {name: 'icon-message-in', content: '2a'}
]

class IconFont extends Component {
    render() {
        const iconName = this.props.iconName;
        const className = this.props.className;
        const icon = icons.find((item => {
            return item.name === iconName
        }));
        var iconStyle = {
            color: this.props.color,
            backgroundColor: this.props.backgroundColor,
            fontSize: scaledSize(this.props.fontSize),
            fontFamily: 'tower-pattern'
        };
        return (
            <Text style={iconStyle}>{hexToAscii(icon.content)}</Text>
        );
    }
}

export default IconFont;