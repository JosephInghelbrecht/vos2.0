import React, {Component} from 'react';
import {Text, TouchableOpacity, StyleSheet} from 'react-native';
import {withNavigation} from "react-navigation";
import {goColor, scaledSize, buttonStyle} from "./helpers/home-style";

const NavigateToButton = (props) => {
        return (
            <TouchableOpacity style={buttonStyle.container}
                              onPress={() => props.navigation.navigate(props.navigateTo)}>
                <Text style={buttonStyle.text}>{props.title}</Text>
            </TouchableOpacity>
        );
};

export default withNavigation(NavigateToButton);

