import React, { Component } from "react";
import { createStackNavigator, createAppContainer } from "react-navigation";

import {
  homeIndexProcedureData,
  fireIndexProcedureData,
  accidentIndexProcedureData,
  gasLeakIndexProcedureData,
  terrorIndexProcedureData,
  bombAlarmIndexProcedureData,
  psychoSocialRiskIndexProcedureData,
  suspiciousObjectIndexProcedureData
} from "../data/ui/index-procedure";

import IndexProcedure from "./index-procedure";
import ViewProcedure from "./view-procedure";
import LoggingIn from "./logging-in";

const AppNavigator = createStackNavigator(
  {
    //First entry by default be our first screen if we do not define initialRouteName
    HomeIndex: {
      screen: IndexProcedure,
      params: {index: homeIndexProcedureData,
                title: 'Veilig op school'}
    },
    FireIndex: {
      screen: IndexProcedure,
      params: {index: fireIndexProcedureData,
                title: 'Brand' }
    },
    AccidentIndex: {
        screen: IndexProcedure,
        params: {index: accidentIndexProcedureData,
                  title: 'Ongeval'}
    },
    GasLeakIndex: {
      screen: IndexProcedure,
      params: {index: gasLeakIndexProcedureData,
                title: 'Gaslek'}
    },
    BombAlarmIndex: {
      screen: IndexProcedure,
      params: {index: bombAlarmIndexProcedureData,
                title: 'Bomalarm'}
    },
    TerrorIndex: {
        screen: IndexProcedure,
        params: {index: terrorIndexProcedureData,
                  title: 'Terrorisme'}
    },
    PsychoSocialRiskIndex: {
        screen: IndexProcedure,
        params: {index: psychoSocialRiskIndexProcedureData,
                  title: 'Psycho-sociaal risico'}
    },
    SuspiciousObjectIndex: {
        screen: IndexProcedure,
        params: {index: suspiciousObjectIndexProcedureData,
                  title: 'Verdacht voorwerp'}
    },
    ViewProcedure : {
        screen: ViewProcedure,
        params: {index: 'NO_INDEX'}
    },
    LoggingIn: {
      screen: LoggingIn
    }
  },
  {
    initialRouteName: "HomeIndex"
  }
);

const App = createAppContainer(AppNavigator);
export default App;
