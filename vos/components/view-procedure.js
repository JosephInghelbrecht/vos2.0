import React, { Component } from "react";
import { Alert, Text, View, FlatList, StyleSheet, TouchableOpacity } from "react-native";
import IconFont from "./helpers/icon-font";
import organisationData from "../data/organisation";
import identityData from "../data/identity";
import procedureList from "../data/procedure.json";
import {
  homeStyle,
  tileStyle,
  goColor,
  scaledSize,
  width
} from "./helpers/home-style";
import { Footer } from "./helpers/basic-elements";
// either import the whole module and call as Communications.method()
import Communications from 'react-native-communications';

class ViewProcedure extends Component {

  renderAction(list, procedureTitle, identity, organisation) {
    if ("action" in list) {
      return (
        <View style={styles.stepAction}>
          {list.action.map((item, index) => (
            this.renderActionTile(item.code, procedureTitle, identity, organisation, index)))
          }
        </View>
      );
    }
  }

  renderList(step, procedureTitle, identity, organisation) {
    if ("list" in step) {
      let list = step.list.map(function (item, index) {
        let counter = index;
        counter++;
        item.id = counter.toString();
        return item;
      })
      return (
        <FlatList style={styles.list}
          data={list}
          renderItem={({ item }) => (
            <View style={styles.step2}>
              <Text style={styles.step2Number}>{item.id}</Text>
              <View style={styles.stepContent}>
                <Text style={styles.stepTitle}>{item.title}</Text>
                {this.renderAction(item, procedureTitle, identity, organisation)}
              </View>
            </View>
          )}
          keyExtractor={item => item.id}
        />
      );
    }
  }

  renderActionTile(code, procedureTitle, identity, organisation, index) {
    let iconName;
    let action = () => { Alert.alert('Action is niet gedefinieerd.'); };
    ;
    if (code === 'SMS') {
      iconName = 'icon-sms';
      action = () => {
        const message = `${procedureTitle}\n${identity.firstName} ${identity.lastName}\n${organisation.name}`;
        //Alert.alert(message);
        Communications.text(organisation.preventieadviseur.phone, message);
      };
    }
    else if (code === 'TEL') {
      iconName = 'icon-phone';
      action = () => { Communications.phonecall(organisation.preventieadviseur.phone, true); };
    }
    else if (code === 'YES') {
      const { navigation } = this.props;
      iconName = 'icon-check-ok';
      action = () => {
        this.props.navigation.navigate("LoggingIn", {
          action: navigation.getParam("action", "NO-ACTION")
        });
      };
    }
    else if (code === 'NO') {
      iconName = 'icon-close';
      action = () => { this.props.navigation.navigate("HomeIndex"); };
    }
    return (
      <View key={index} style={styles.actionTile}>
        <TouchableOpacity
          onPress={() => {
            action();
          }}
        >
          <IconFont
            iconName={iconName}
            fontSize={styles.actionTile.fontSize}
            color={styles.actionTile.color}
            backgroundColor={styles.actionTile.backgroundColor}
            margin={styles.actionTile.margin}
          />
        </TouchableOpacity>
      </View>);
  }

  render() {
    /* 2. Get the param, provide a fallback value if not available */
    const { navigation } = this.props;
    const procedureCode = navigation.getParam("code", "NO-CODE");
    const procedureTitle = navigation.getParam("title", "algemeen alarm");
    const organisation = navigation.getParam("organisationData", organisationData);
    const identity = navigation.getParam("identityData", identityData)
    let idCounter = 0;
    let role = procedureList.role.find(function (item) {
      return item.code === identity.role;
    });
    // Alert.alert(JSON.stringify(role));

    let procedure = role.procedure.find(function (item) {
      return item.code === procedureCode;
    });
    let steps = procedure.step.map(function (item, index) {
      let counter = index;
      counter++;
      item.id = counter.toString();
      return item;
    })
    return (
      <View style={homeStyle.container}>
        <View>
          <Text style={homeStyle.welcome}>{procedureTitle}</Text>
        </View>
        <FlatList style={styles.list}
          data={steps}
          showsVerticalScrollIndicator={false}
          renderItem={({ item }) => (
            <View>
              <View style={styles.step}>
                <Text style={styles.stepNumber}>{item.id}</Text>
                <View style={styles.stepContent}>
                  <Text style={styles.stepTitle}>{item.title}</Text>
                  {this.renderAction(item, procedureTitle, identity, organisation)}
                </View>
              </View>
              <View>
                {this.renderList(item, procedureTitle, identity, organisation)}
              </View>
            </View>)
          }
          keyExtractor={item => item.id}
        />
        <Footer />
      </View>
    );
  }
}
export const styles = StyleSheet.create({
  list: {
    flexDirection: "column",
  },
  step: {
    marginTop: scaledSize(6),
    marginRight: scaledSize(4),
    borderStyle: "solid",
    borderTopColor: goColor.magenta,
    borderTopWidth: 1,
    flexDirection: "row"
  },
  stepContent: {
    flexDirection: "column"
  },
  stepAction: {
    flexDirection: "row"
  },
  stepTitle: {
    paddingTop: scaledSize(7),
    paddingBottom: scaledSize(7),
    paddingRight: scaledSize(7),
    paddingLeft: scaledSize(7),
    margin: 0,
    maxWidth: width - 50
  },
  actionTile: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: goColor.magenta,
    margin: scaledSize(7),
    padding: scaledSize(6),
    borderRadius: scaledSize(5),
    fontSize: scaledSize(40),
    color: 'white'
  },
  stepNumber: {
    color: "white",
    marginRight: scaledSize(2),
    paddingTop: scaledSize(7),
    paddingLeft: 7,
    backgroundColor: goColor.darkgray,
    width: 31,
  },
  step2: {
    marginRight: scaledSize(4),
    borderStyle: "solid",
    borderTopColor: goColor.orange,
    borderTopWidth: 1,
    borderLeftColor: goColor.darkgray,
    borderLeftWidth: 4,
    flexDirection: "row"
  },
  step2Number: {
    color: "white",
    marginRight: scaledSize(2),
    paddingTop: scaledSize(7),
    paddingLeft: 7,
    backgroundColor: goColor.orange,
    width: 27
  }
});

export default ViewProcedure;
