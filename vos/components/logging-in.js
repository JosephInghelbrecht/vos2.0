import React, { Component } from 'react';
import { Alert, View, Text, TextInput, StyleSheet, TouchableOpacity } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { goColor, homeStyle, scaledSize, buttonStyle } from "./helpers/home-style";
import { Label } from "./helpers/basic-elements";
import personList from '../data/personList.json';

class LoggingIn extends Component {
    userName = '';
    password = '';

    setIdentityData = async (identityData) => {
        try {
            await AsyncStorage.setItem('@identityData', JSON.stringify(identityData));
            this.props.navigation.navigate('HomeIndex');

        } catch (e) {
            // saving error
            Alert.alert(e);
        }
    }

    updateIdentityData(value) {
        const { navigation } = this.props;
        const action = navigation.getParam("action", "NO-ACTION");
        // calls the setIdentityData method in IndexProcedure
        action(value);
        navigation.navigate("HomeIndex");
    }

    login() {
        // je kan blijkbaar this niet in map gebruiken ,je moet this
        // meegeven als tweede argument aan de find functies
        // namelijk als Object to use as this when executing callback
        if (this.userName) {
            let identity = personList.list.find(function (item) {
                // Alert.alert('test' + item.userName + this.userName);
                return item.userName === this.userName && item.password === this.password
            }, this);
            if (identity) {
                identity.loggedIn = true;
                this.updateIdentityData(identity);
                // Alert.alert('Je bent aangemeld als ' + this.userName);
              } else {
                Alert.alert('Niet bestaande gebruikersnaam opgeven!');
            }
        } else {
            Alert.alert('Je moet een gebruikersnaam opgeven!');
        }
    };

    render() {
        return (
            <View style={styles.container}>
                <Text style={homeStyle.welcome}>Aanmelden</Text>
                <Label title="Gebruikersnaam: " />
                <TextInput style={homeStyle.textInput}
                    autoCapitalize="none"
                    autoCorrect={false}
                    returnKeyType="next"
                    onChangeText={(text) => this.userName = text} />

                <Label title="Paswoord: " />
                <TextInput style={homeStyle.textInput}
                    autoCapitalize="none"
                    returnKeyType="go"
                    secureTextEntry={true}
                    onChangeText={(text) => this.password = text} />

                <TouchableOpacity style={buttonStyle.container}
                    onPress={() => this.login()}>
                    <Text style={buttonStyle.text}>Aanmelden</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: scaledSize(10)
    }
});

export default LoggingIn;